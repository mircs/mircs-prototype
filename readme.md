# Catalyst [![Build Status](https://travis-ci.org/haydenbleasel/catalyst.svg)](https://travis-ci.org/haydenbleasel/catalyst)

Catalyst is a task automation boilerplate for experienced web developers. It's used for creating powerful, efficient and scalable websites by handling all the repetitive work for you, allowing you to focus on the code rather than the trivia.  Think of it like the [HTML5 Boilerplate](http://html5boilerplate.com/) for building and previewing your website in the most efficient way possible.

## Requirements

You'll need [Node.js](http://nodejs.org/), ImageMagick, GraphicsMagick and PhantomJS installed. If you're using [Homebrew](http://brew.sh/), this is as easy as:

    brew install node imagemagick graphicsmagick phantomjs

## Build System

Catalyst doesn't include any pre-processors or templating engines by default (Jade, Less, CoffeeScript, etc) because these will differ on your application. HTML, CSS and JavaScript are the assumed defaults so you'll need to change these if you want extra power.

### Templates

Templates are run against the W3C validator and references to non-optimised assets are replaced with their compiled, revisioned counterparts. The templates are then heavily minified.

### Stylesheets

Stylesheets are cross-referenced with your templates and unused CSS is removed. Once this is complete, vendor prefixes are added to the compiled stylesheets and your media queries are combined. Finally, everything's minified then split into multiple stylesheets for old Internet Explorer support.

### Scripts

All development code (console logs, alerts, etc) is removed from the scripts before being heavily minified.

### Images

Images are highly optimised and multiple sizes of the image are created for better performance on mobile devices (1200px, 992px and 768px by default). Images that you've specified will be added to a CSS sprite during the build process. For both functions, the relevant CSS is created for you.

### Miscellaneous

You might notice there's quite a few files missing from the repository i.e. a sitemap, robots.txt, favicon and icons for iOS, Android, Windows 8 and Facebook (Open Graph). These are all generated for you during the build process.

## Download and Usage

Getting Catalyst is easy. You can either [download the repo](https://github.com/haydenbleasel/catalyst/archive/master.zip) directly or clone it from GitHub. I recommend the following snippet:

    git clone --depth 1 --branch master https://github.com/haydenbleasel/catalyst.git <yourprojectname>

Catalyst isn't very difficult to use but, like any boilerplate, it does require a bit of configuration in the beginning (all in the Gulpfile.js). There are a few functions you'll need to know:

Build your website:

    gulp

Boot up the live preview server:

    gulp preview

Clean up the project:

    gulp clean (--tmp or --dist)

Bump your version number:

    gulp bump (--major, --minor or --patch)

Check your HTML for broken links:

    gulp check

Check the size of your distribution:

    gulp size

Test your website with Pagespeed Insights:

    gulp pagespeed (--mobile or --desktop)

## Support

For issues and contributions to the repository, head over to the [repository](https://github.com/haydenbleasel/catalyst) page on GitHub. For help or feedback, contact me on [Twitter](https://twitter.com/haydenbleasel) for a quicker response.

## Recipes

Typically you'll want some sort of pre-processing. These are some common recipes:

### Jade

    gulp.task('jade', function () {
        return gulp.src('source/*.jade')
            .pipe($.jade({ pretty: true }))
            .pipe(gulp.dest('.tmp'))
            .pipe(sync.reload({ stream: true, once: true }));
    });

### Less (requires Less, Recess and Plumber)

    gulp.task('less', function () {
        return gulp.src('source/styles/**/*.less')
            .pipe($.plumber({
                errorHandler: function (err) {
                    $.util.beep();
                    $.util.log($.util.colors.red(err.plugin + ': ' + err.message));
                }
            }))
            .pipe($.recess())
            .pipe($.less())
            .pipe(gulp.dest('.tmp/concat'))
            .pipe(sync.reload({ stream: true, once: true }));
    });

### Coffeescript

    gulp.task('coffeescript', function () {
        return gulp.src('source/scripts/**/*.coffee')
            .pipe($.coffeelint())
            .pipe($.coffeelint.reporter())
            .pipe($.coffee())
            .pipe(gulp.dest('.tmp/scripts'))
            .pipe(sync.reload({ stream: true, once: true }));
    });

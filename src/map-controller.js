angular.module('app')

.controller('MapController', function($scope, $locale, $timeout, Spreadsheets, $q, $anchorScroll, $location) {

  $scope.map = {
    center: {
      latitude: 44.656960755560725,
      longitude: -63.57836123476562
    },
    zoom: 14,
    bounds: {}
  };

  $scope.mapOptions = {
    scrollwheel: false
  };

  $scope.gridOptions = {};

  $scope.dropDownOptions = [];
  $scope.dropDownSelected = {
    gridOptions: {}
  };

  var addressMap = {};

  $scope.lastQuery = {
    expr: new RegExp()
  };

  $scope.filterFunction = function(marker) {
    if (!$scope.searchQuery || $scope.searchQuery.length === 0) {
      return true;
    }

    if ($scope.lastQuery.searchQuery !== $scope.searchQuery) {
      $scope.lastQuery.searchQuery = $scope.searchQuery;
      $scope.lastQuery.expr = new RegExp('(?=.*' + $scope.searchQuery.split(' ').join(')(?=.*') + ')', 'gi');
    }

    var match = $scope.lastQuery.expr.exec(marker.search);
    return match;
  };

  var promises = [];

  promises.push(Spreadsheets.northEndCivicsJson()
    .then(function(historicAddresses) {
      console.log('done NorthEndCivics_updated');

      var markers = [];
      var i = 0;
      _.each(historicAddresses, function(record) {
        var address = {
          addressString: [record['FORMER CIVIC'], record['FORMER STREET'].split(' ')[0]].join(' '),
          latitude: record['LATITUDE'],
          longitude: record['LONGITUDE']
        };

        addressMap[address.addressString] = address;

        // show all markers, 10,000:
        // var marker = createMarker(address, ++i, $scope.map.bounds);
        // markers.push(marker);
        // marker.row = record;

        // if (address.latitude) {
        //   var marker = createMarker(address, ++i, $scope.map.bounds);
        //   var row = addressMap[marker.title];
        //   if (row) {
        //     markers.push(marker);
        //     marker.row = row;
        //     marker.search = _.values(row).join(' ');
        //   }
        // }
      });

      $scope.addressMarkers = markers;
    }));

  promises.push(Spreadsheets.stPats()
    .then(function(spreadsheet) {
      console.log('done St. Pat\'s Sample Census');

      $scope.samples = spreadsheet;
    }));

  $q.all(promises).then(function() {
    console.log('done $q.all');
    var markers = [];

    var count = 0;
    var misses = 0;

    _.each($scope.samples['Sara\'s Sample'], function(row) {
      var addressString = [row['Street Number'], row['Address'].split(' ')[0]].join(' ').toUpperCase();
      var address = addressMap[addressString];
      if (address) {
        var marker = createMarker(address, ++count, $scope.map.bounds);
        if (row) {
          markers.push(marker);
          marker.row = row;
          marker.search = _.values(row).join(' ');
        }
        // console.log('HIT', count, addressString);
      } else {
        ++misses;
        // console.log('MISS', misses, addressString);
      }
    });

    console.log('Found addresses:', count, 'Missed addresses:', misses);

    $scope.addressMarkers = markers;
  });

  function createMarker(address, markerId, bounds) {
    var latitude = address.latitude;
    var longitude = address.longitude;

    var marker = {
      latitude: latitude,
      longitude: longitude,
      title: address.addressString,
      show: false
    };
    marker.onClick = function() {
      console.log(marker);
      $scope.selectedMarker = marker;
      $scope.gotoAnchor(marker.id);
    };
    marker.id = markerId;
    return marker;
  };

  $scope.rowToString = function(row) {
    var string = '';
    _.each(row, function(value, key) {
      if (key !== 'undefined') {
        string += '  ' + key + ': ' + value + '\n';
      }
    })
    return string;
  }

  $scope.onMarkerRowClicked = function(marker) {
    console.log(marker);
    $scope.selectedMarker = marker;
  };

  $anchorScroll.yOffset = 50; // always scroll by 50 extra pixels
  $scope.gotoAnchor = function(x) {
    var newHash = 'anchor' + x;
    if ($location.hash() !== newHash) {
      // set the $location.hash to `newHash` and
      // $anchorScroll will automatically scroll to it
      $location.hash('anchor' + x);
    } else {
      // call $anchorScroll() explicitly,
      // since $location.hash hasn't changed
      $anchorScroll();
    }
  };

});

angular.module('app').factory('Spreadsheets', ['$q', function($q) {
  function loadSpreadsheet(url) {
    var deferred = $q.defer();

    var oReq = new XMLHttpRequest();
    oReq.open('GET', url, true);
    oReq.responseType = 'arraybuffer';

    oReq.onload = function(e) {
      var arraybuffer = oReq.response;

      // convert data to binary string
      var data = new Uint8Array(arraybuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join('');

      var workbook = XLSX.read(bstr, {
        type: 'binary'
      });
      deferred.resolve(workbook);
    }
    oReq.send();

    return deferred.promise;
  }

  function loadSpreadsheetAsJson(url, sheetNumber) {
    return loadSpreadsheet(url).then(function(workbook) {
      var json = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[sheetNumber]]);
      return json;
    });
  }

  function loadAllSheetAsJson(url) {
    return loadSpreadsheet('files/St. Pat\'s Sample Census template (March 24th, 2015 version).xlsx')
      .then(function(spreadsheet) {
        var json = {};
        _.each(spreadsheet.SheetNames, function(sheetName) {
          json[sheetName] = XLSX.utils.sheet_to_json(spreadsheet.Sheets[sheetName]);
        });
        return json;
      });
  }

  var _cached = {};

  var Spreadsheets = {
    loadSpreadsheet: loadSpreadsheet,
    loadSpreadsheetAsJson: loadSpreadsheetAsJson,

    // data:

    // multi-sheet data from Breanna:
    stPats: function() {
      if (!_cached.stPats) {
        _cached.stPats = loadAllSheetAsJson('files/St. Pat\'s Sample Census template (March 24th, 2015 version).xlsx');
      }
      return _cached.stPats;
    },

    // single-sheet data from Halifax Water, headings:
    // FULL CIVIC
    // CIVIC NUMBER
    // STREET
    // COMMUNITY
    // FORMER CIVIC
    // FORMER STREET
    // FORMER COMMUNITY
    // CHANGE DATE
    // EASTING
    // NORTHING
    // LATITUDE
    // LONGITUDE
    northEndCivicsJson: function() {
      if (!_cached.northEndCivicsJson) {
        _cached.northEndCivicsJson = loadSpreadsheetAsJson('files/NorthEndCivics_updated.xlsx', 0)
      }
      return _cached.northEndCivicsJson;
    }
  };
  return Spreadsheets;
}]);

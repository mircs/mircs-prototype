angular.module('app', ['uiGmapgoogle-maps', 'ui.router'])

.config(function(uiGmapGoogleMapApiProvider) {
  uiGmapGoogleMapApiProvider.configure({
    key: 'AIzaSyCdy3FcPNyPfwPf7_vN_Fyz7sWmr_b2b7M',
    v: '3.17',
    libraries: 'weather,geometry,visualization'
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /map
  $urlRouterProvider.otherwise("/map");
  //
  // Now set up the states
  $stateProvider
    .state('map', {
      url: "/map",
      templateUrl: "partials/map.html"
    })
    .state('historical-map', {
      url: "/historical-map",
      templateUrl: "partials/historical-map.html"
    })
    .state('profiles', {
      url: "/profiles",
      templateUrl: "partials/profiles.html"
    })
    .state('profiles.John-Davison', {
      url: "/profiles.John-Davison",
      templateUrl: "partials/profiles.John-Davison.html"
    })
    .state('profiles.Robert-A-Guilford', {
      url: "/profiles.Robert-A-Guilford",
      templateUrl: "partials/profiles.Robert-A-Guilford.html"
    })
    .state('profiles.Schwartzes', {
      url: "/profiles.Schwartzes",
      templateUrl: "partials/profiles.Schwartzes.html"
    });
});

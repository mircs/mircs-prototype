/*jslint node:true*/

(function () {

    'use strict';

    var gulp = require('gulp'),
        stream = require('event-stream'),
        sync = require('browser-sync'),
        pagespeed = require('psi'),
        del = require('del'),
        yargs = require('yargs').argv,
        open = require('open'),
        Pageres = require('pageres'),
        crawler = require('simplecrawler'),
        $ = require('gulp-load-plugins')({
            pattern: ['gulp-*', 'gulp.*'],
            replaceString: /\bgulp[\-.]/
        }),
        config = require('./package.json');

    gulp.task('clean', function (next) {
        if (yargs.tmp) {
            return del('build/.tmp/', next);
        }
        if (yargs.dist) {
            return del('build/dist/', next);
        }
        return del(['build/.tmp/', 'build/dist/'], next);
    });

    gulp.task('copy', function () {
        return stream.concat(
            gulp.src('source/fonts/*')
                .pipe(gulp.dest('build/.tmp/fonts/')),
            gulp.src('src/**/*.html')
                .pipe(gulp.dest('build/.tmp/')),
            gulp.src('src/images/**/*')
                .pipe(gulp.dest('build/.tmp/images/')),
            gulp.src('src/**/*.js')
                .pipe(gulp.dest('build/.tmp/')),
            gulp.src('src/**/*.css')
                .pipe(gulp.dest('build/.tmp/')),
            gulp.src('src/**/*.xlsx')
                .pipe(gulp.dest('build/.tmp/'))
        );
    });

    gulp.task('sprites', ['copy'], function () {
        var sprites = [];
        if (sprites.length > 0) {
            var spriteData = gulp.src(sprites)
                .pipe($.spritesmith({
                    imgName: 'sprite.png',
                    cssName: 'sprite.css',
                    engine: 'phantomjs'
                }));
            spriteData.img.pipe(gulp.dest('build/.tmp/images/'));
            spriteData.css.pipe(gulp.dest('build/.tmp/concat/'));
        }
    });

    gulp.task('styles', ['sprites'], function () {
        return gulp.src(['src/styles/**/*.css', 'build/.tmp/concat/**/*.css'])
            .pipe($.concat('main.css'))
            .pipe($.autoprefixer())
            .pipe(gulp.dest('build/.tmp/styles/'))
            .pipe(sync.reload({ stream: true, once: true }));
    });

    gulp.task('bundle', ['styles'], function () {
        var assets = $.useref.assets();
        return gulp.src('build/.tmp/**/*.html')
            .pipe($.robots({ out: 'build/dist/robots.txt' }))
            .pipe($.humans({ header: 'MIRCS', out: 'build/dist/humans.txt' }))
            // .pipe($.favicons({
            //     files: {
            //         dest: '../dist/',
            //         iconsPath: '/'
            //     },
            //     settings: {
            //         background: '#4A90E2',
            //         version: config.version,
            //         index: 'index.html'
            //     }
            // }))
            .pipe(assets)
            .pipe($.rev())
            .pipe(assets.restore())
            .pipe($.useref())
            .pipe($.revReplace())
            .pipe(gulp.dest('build/dist/'));
    });

    gulp.task('finalise', ['sprites'], function () {
        return stream.concat(
            gulp.src(['source/**/*.txt', 'source/.htaccess'])
                .pipe(gulp.dest('build/dist/')),
            gulp.src('source/fonts/*')
                .pipe(gulp.dest('build/dist/fonts/')),
            gulp.src(['build/.tmp/images/**/*'])
                .pipe(gulp.dest('build/dist/images/'))
        );
    });

    gulp.task('compress', ['bundle', 'finalise'], function () {
        var header = '/*! Copyright 2015 MIRCS. */';
        return stream.concat(
            gulp.src('build/dist/**/*.html')
                .pipe($.minifyHtml({ quotes: true }))
                .pipe(gulp.dest('build/dist/'))
                .pipe($.sitemap({ siteUrl: 'http://mircs.ca/database-prototype' }))
                .pipe(gulp.dest('build/dist/')),
            gulp.src('build/dist/**/*.css')
                .pipe($.header(header))
                .pipe($.rwi({ img: 'build/.tmp/images/' }))
                .pipe($.uncss({ html: ['build/dist/index.html'] }))
                .pipe($.combineMediaQueries())
                .pipe($.autoprefixer())
                .pipe($.csso())
                .pipe($.bless())
                .pipe(gulp.dest('build/dist/')),
            gulp.src('build/dist/**/*.js')
                .pipe($.stripDebug())
                .pipe($.uglify())
                .pipe($.header(header))
                .pipe(gulp.dest('build/dist/'))
        );
    });

    gulp.task('default', ['clean'], function () {
        return gulp.start('compress');
    });

    gulp.task('preview', ['watch'], function () {
        return open('http://localhost:3000/');
    });

    gulp.task('browser-sync', ['styles'], function () {
        return sync.init(['*.html', 'styles/**/*.css', 'scripts/**/*.js', 'images/**/*'], {
            server: { baseDir: 'build/.tmp/' },
            open: false,
            tunnel: true
        });
    });

    gulp.task('watch', ['browser-sync'], function () {
        gulp.watch(['src/**/*'], ['styles']);
    });

    gulp.task('bump', function () {
        var type;
        if (yargs.major) {
            type = "major";
        } else if (yargs.minor) {
            type = "minor";
        } else if (yargs.patch) {
            type = "patch";
        }
        return gulp.src(['bower.json', 'package.json'])
            .pipe($.bump({ type: type }))
            .pipe(gulp.dest('./'));
    });

    gulp.task('pagespeed', ['browser-sync'], function () {
        var strategy = yargs.mobile ? 'mobile' : 'desktop';
        sync.emitter.on("service:running", function (data) {
            pagespeed({ nokey: 'true', url: data.tunnel, strategy: strategy }, sync.exit);
        });
    });

    gulp.task('check', ['browser-sync'], function () {
        sync.emitter.on("service:running", function (data) {
            crawler.crawl(data.tunnel)
                .on("fetchcomplete", function (link) {
                    $.util.log('Download successful: ' + link.url);
                })
                .on("fetcherror", function (link) {
                    $.util.log('Download unsuccessful: ' + link.url);
                })
                .on("complete", sync.exit);
        });
    });

    gulp.task('responsive', ['browser-sync'], function () {
        var resolutions = ['1920x1080', '1680x1050', '768x1024', '320x480'];
        if (resolutions.length > 0) {
            sync.emitter.on("service:running", function (data) {
                var pageres = new Pageres()
                    .src(data.tunnel, resolutions, { crop: true })
                    .dest('build/screenshots/');
                pageres.run(function (error) {
                    if (error) {
                        throw error;
                    }
                    $.util.log('Successfully created screenshots');
                    sync.exit();
                });
            });
        }
    });

    gulp.task('size', ['default'], function () {
        return gulp.src('build/dist/**/*')
            .pipe($.size({ showFiles: true }));
    });

}());
